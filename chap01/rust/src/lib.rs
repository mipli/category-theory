pub fn id<T>(x: T) -> T {
    x
}

pub fn compose<A, B, C>(
    f: impl Fn(A) -> B + 'static,
    g: impl Fn(B) -> C + 'static,
) -> Box<dyn Fn(A) -> C> {
    Box::new(move |x| g(f(x)))
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_identity() {
        for i in 1..10 {
            assert_eq!(id(i), i);
        }
    }

    #[test]
    fn test_compose() {
        let f = |x| x + 5;
        let g = |x| x + 3;
        let c = compose(f, g);
        for i in 1..10 {
            assert_eq!(c(i), f(g(i)));
        }
    }

    #[test]
    fn test_compose_id() {
        let f = |x| x + 5;
        let f_id = compose(f, id);
        let id_f = compose(id, f);
        for i in 1..10 {
            assert_eq!(f_id(i), f(i));
            assert_eq!(id_f(i), f(i));
        }
    }
}
