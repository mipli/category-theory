Code solutions to the challenges [Category Theory for Programmers](https://github.com/hmemcpy/milewski-ctfp-pdf). My
thoughts and comments on the book is otherwise available at [Kindled Learning](https://mipli.gitlab.io).

