use std::collections::{hash_map::Entry, HashMap};

fn fib(n: i32) -> i32 {
    match n {
        0 | 1 => 1,
        _ => fib(n - 2) + fib(n - 1),
    }
}

struct Memo {
    func: Box<dyn Fn(i32) -> i32>,
    // Should use a better hashing function than the default one here to get better benefits,
    // but that isn't really the focus for this exercise
    mem: HashMap<i32, i32>,
}

impl Memo {
    fn new(func: impl Fn(i32) -> i32 + 'static) -> Self {
        Memo {
            func: Box::new(func),
            mem: HashMap::default(),
        }
    }

    pub fn call(&mut self, val: i32) -> i32 {
        match self.mem.entry(val) {
            Entry::Occupied(entry) => {
                dbg!("cached!");
                *entry.get()
            }
            Entry::Vacant(entry) => {
                dbg!("calculating", val);
                let res = (*self.func)(val);
                entry.insert(res);
                res
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn slow() {
        assert_eq!(fib(10), 89);
    }

    #[test]
    fn memoized() {
        let mut memo = Memo::new(fib);
        assert_eq!(memo.call(10), 89);
    }

    #[test]
    fn speed() {
        let mut memo = Memo::new(fib);
        let nums = 20;
        let slow = {
            let start = std::time::Instant::now();
            for _ in 0..nums {
                let _ = fib(nums);
            }
            std::time::Instant::now() - start
        };
        let fast = {
            let start = std::time::Instant::now();
            for _ in 0..nums {
                let _ = memo.call(nums);
            }
            std::time::Instant::now() - start
        };

        dbg!(fast);
        dbg!(slow);

        assert!(fast < slow);
    }
}
