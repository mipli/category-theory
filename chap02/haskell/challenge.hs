-- Finds fixed point of a function
fix :: (a -> a) -> a
fix f = let x = f x in x

-- Fibonacci sequence, using a supplied function to find the next values
fib :: (Int -> Int) -> (Int -> Int)
fib f 0 = 0
fib f 1 = 1
fib f n = f (n - 2) + f (n - 1)

-- Multiplier, using a supplied function to find the previous value
geom :: (Int -> Int) -> (Int -> Int)
geom f 1 = 1
geom f 2 = 2
geom f n =  n * f (n - 2) + n * f (n - 1)

-- Memoization function, assuming we can use an integer to index the result we want
memoize :: (Int -> a) -> (Int -> a)
memoize f = (map f [0 ..] !!)

-- Slow implemenetation of the Fibonacci and Multiplier
fib_slow = fib fib_slow
geom_slow = geom geom_slow

-- Memoized implemenetation of the Fibonacci and Multiplier
fib_memo = fix (memoize . fib)
geom_memo = fix (memoize . geom)
